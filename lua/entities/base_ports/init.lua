AddCSLuaFile('shared.lua')
AddCSLuaFile('cl_init.lua')

include('shared.lua')

function ENT:InitializePorts()
	self.Ports = {}
	self.Ports["Output"] = {}
	self.Ports["Input"] = {}
end
-------------------------------------------------------------------------
-- Adds A Port to the specified entity.
-- You can set the port to a value( output ) s
-- or reference( connect/input ) the port using a tool.
-------------------------------------------------------------------------
function ENT:AddPort( PortName, PortType, InitialValue )
	if( type(PortName) != "string") then
		print("Error cannot create a port with a non string name!")
		return false
	else
		if self.Ports[PortType] and self.Ports[PortType][PortName] then
			print("Error cannot create a port with the same name and type!")
			return false
		end
	end
	
	if not ioLibrary.PortTypes[PortType] then print("invalid type") return false end
	
	local port = table.Copy( ioLibrary.portStructure )
	port.Value = InitialValue
	port.Type = PortType
	port.Entity = self
	self.Ports[PortType][PortName] = port
	-------------------------------------------------------------------------
	-- Returns the reference to the port as they might want to
	-- update or add there own functions.
	-------------------------------------------------------------------------
	return port
end

-------------------------------------------------------------------------
-- Removes a port
-- Simple eh?
-------------------------------------------------------------------------
function ENT:RemovePort( PortName, PortType )
	self.Ports[PortType][PortName] = nil
end

-------------------------------------------------------------------------
-- Returns a port with the specified Name and Type.
-- otherwise it will return false!
-------------------------------------------------------------------------
function ENT:GetPort( PortName, PortType )
	if self.Ports[PortType] == nil then return false end
	return self.Ports[PortType][PortName] or false
end

-------------------------------------------------------------------------
-- Returns all Input Type ports on the entity.
-------------------------------------------------------------------------
function ENT:GetInputs()
	return self.Ports["Input"]
end

-------------------------------------------------------------------------
-- Returns all Output Type ports on the entity.
-------------------------------------------------------------------------
function ENT:GetOutputs()
	return self.Ports["Output"]
end

-------------------------------------------------------------------------
-- Is executed when an input is update.
-- The value has already been changed at this point.
-- The port perameter is always the local port that has changed.
-- You can do port.ConnectedOutput it should give you the connected value.
-------------------------------------------------------------------------
function ENT:TriggerInputValue( port, entity )
	-- Overwrite this function to do what you want.
end

-------------------------------------------------------------------------
-- Execute this when you change the value of a port. ( The value is an optional perameter. )
-- 
-------------------------------------------------------------------------
function ENT:UpdateLocalOutput( PortName, val )
	if PortName != nil and PortName != "" and PortName != " " then
		if self.Ports["Output"] then
			local thePort = self.Ports["Output"][PortName]
			if thePort == nil then return nil end -- a non existant port.

			if val == nil then
				for k,v in pairs( thePort.ConnectedInputs ) do
					v.Entity:TriggerInputValue( v, k )
				end
			else
				for k,v in pairs( thePort.ConnectedInputs ) do
					v.Value = val
					v.Entity:TriggerInputValue( v, k )
				end
			end
			
			return true
		end
	else

		return false
	end
end