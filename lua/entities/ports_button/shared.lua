ENT.Type = "anim"
ENT.Base = "base_ports"

ENT.Name = "Button"
ENT.PrintName = "True Ports Button"
ENT.Author = "RevoluPowered"
ENT.Contact = "revolupowered@googlemail.com"
ENT.Purpose = "A button entity."

ENT.Information = "A button entity."
ENT.Category = "Ports by GordonM"

ENT.Spawnable = true
ENT.AdminSpawnable = true