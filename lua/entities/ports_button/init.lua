AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_init.lua")
include( 'shared.lua' )

function ENT:Initialize()
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( CONTINUOUS_USE )

	self:InitializePorts()
	self.State = false
	self.PortType = "Simple"
	local theport = self:AddPort( "Value", "Output", self.State  )
end
--for debug 
function ENT:SpawnFunction( ply, trace )
	if(trace.HitPos == nil or trace.HitNormal == nil) then return end
	local theEnt = ents.Create( "ports_button" )
	undo.Create("prop")
		undo.AddEntity( theEnt )
		undo.SetPlayer( ply )
	undo.Finish()
	theEnt:SetModel("models/Squad/sf_tris/sf_tri1x1.mdl")
	theEnt:SetPos( trace.HitPos + (trace.HitNormal * 5) )
	theEnt:Spawn()
	theEnt:Activate()
	return theEnt
end

function ENT:Think()
	return
end

function ENT:Use( activator, caller )
	self.State = !self.State
	print("\n\nUSE")
end
