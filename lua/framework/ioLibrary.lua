-------------------------------------------------------------------------
-- ioLibrary
-- Author: Gordon MacPherson
-- Why: Shit didn't make sense :P
-- O(1) Assistance: Divran.
-- License: GNU/Linux Open Source.
-------------------------------------------------------------------------
ioLibrary = {}
ioLibrary.portStructure = 
{
	Name,
	Value,
	Type,
	ConnectedOutput,
	ConnectedInputs = {}
}
ioLibrary.PortTypes = 
{
	Input = true, 
	Output = true
}
ioLibrary.EntityBase = 
{
	Ports =	{}
}
ioLibrary.EntityBase.Ports["Input"] ={}
ioLibrary.EntityBase.Ports["Output"] = {}

-------------------------------------------------------------------------
-- Connect Input to Output
-- Connects them together.
-------------------------------------------------------------------------
function ioLibrary.Connect( porta, portb )
	if porta ~= nil and portb ~= nil then
		if porta.Type ~= portb.Type then
			if type(porta.Value) ~= type(portb.Value) then return false end
			if(porta.Type == "Output") then
				
				porta.ConnectedInputs[portb.Entity] = portb
				portb.ConnectedOutput = porta
			else
				portb.ConnectedInputs[porta.Entity] = porta
				porta.ConnectedOutput = portb
			end
			return true
		else
			return false
		end
	else
		return false
	end
end

-------------------------------------------------------------------------
-- Disconnects a Input from an Output.
-- They are still useable though.
-------------------------------------------------------------------------
function ioLibrary.Disconnect( porta, portb )
	if Output ~= nil and Input ~= nil then
		if(porta.Type == "Output") then
			if( porta.ConnectedInputs[portb.Entity] == nil and portb.ConnectedOutput == nil ) then return false end
			porta.ConnectedInputs[portb.Entity] = nil
			portb.ConnectedOutput = nil
		else
			if( portb.ConnectedInputs[porta.Entity] == nil and porta.ConnectedOutput == nil ) then return false end
			porta.ConnectedOutput = nil
			portb.ConnectedInputs[porta.Entity] = nil
		end
		return true
	else
		return false
	end
end

-------------------------------------------------------------------------
-- IsConnected
-- Is this port connected to anything? ( Works on inputs and outputs. )
-------------------------------------------------------------------------
function ioLibrary.isConected( port )
	if port != nil and port then
		if port.ConnectedOutput ~= nil then
			return true
		end
		
		if(next(port.ConnectedInputs) ~= nil) then
			return true
		end
		
		return false
	else
		return nil
	end
end

-------------------------------------------------------------------------
-- returns the port type.
-- e.g. Input or Output
-------------------------------------------------------------------------
function ioLibrary.GetPortType( port )
	return port.Type
end

-------------------------------------------------------------------------
-- returns true if the port has a valid type.
-- e.g. Input or Output
-------------------------------------------------------------------------
function ioLibrary.validType( PortType )
	return ioLibrary.PortTypes[PortType] ~= nil
end

