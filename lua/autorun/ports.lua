if VERSION < 151 then
	error("Sorry you cannot use this addon in an old version of gmod.")
end

AddCSLuaFile('ports.lua')

ports = {}
ports.version = 0.1

function ports:Initialize()
	print("Initialising Ports, Version: " .. ports.version)

	if SERVER then
		print("Server Initialized!")
		include("framework/ioLibrary.lua")
	end
	
	if CLIENT then
		print("Client Initialized!")
	end
end


ports:Initialize()